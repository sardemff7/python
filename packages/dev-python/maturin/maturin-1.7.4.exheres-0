# Copyright 2023 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require cargo [ rust_minimum_version=1.74.0 ]
require pypi
require py-pep517 [ backend=setuptools test=pytest entrypoints=[ ${PN} ] ]
require flag-o-matic

SUMMARY="Build and publish crates as well as rust binaries as python packages"
DESCRIPTION="
Build and publish crates with pyo3, rust-cpython, cffi and uniffi bindings as
well as rust binaries as python packages.
This project is meant as a zero configuration replacement for setuptools-rust
and milksnake. It supports building wheels for python 3.5+ on windows, linux,
mac and freebsd, can upload them to pypi and has basic pypy support."

LICENCES=" || ( Apache-2.0 MIT )"
SLOT="0"
PLATFORMS="~amd64 ~armv8"
MYOPTIONS=""

DEPENDENCIES="
    build:
        dev-python/setuptools-rust[>=1.4.0][python_abis:*(-)?]
    build+run:
        python_abis:3.8? ( dev-python/tomli[>=1.1.0][python_abis:3.8] )
        python_abis:3.9? ( dev-python/tomli[>=1.1.0][python_abis:3.9] )
        python_abis:3.10? ( dev-python/tomli[>=1.1.0][python_abis:3.10] )
    test:
        dev-python/boltons[python_abis:*(-)?]
        dev-python/cffi[python_abis:*(-)?]
"

# Needs network, TODO: avoid that
RESTRICT="test"

unpack_one_multibuild() {
    default

    edo pushd "${PYTHON_WORK}"
    ecargo_fetch
    edo popd
}

configure_one_multibuild() {
    # Undefined references to 'GFp_cpuid_setup' for example
    filter-flags -flto

    py-pep517_configure_one_multibuild
}

test_one_multibuild() {
    export MATURIN_TEST_PYTHON=${PYTHON}

    cargo_src_test
}

