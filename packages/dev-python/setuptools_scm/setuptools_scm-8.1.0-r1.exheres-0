# Copyright 2016-2024 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require pypi
require py-pep517 [ backend=self-hosted blacklist=2 ]

SUMMARY="Manage your versions by scm tags"

LICENCES="MIT"
SLOT="0"
PLATFORMS="~amd64 ~armv7 ~armv8 ~x86"
MYOPTIONS=""

DEPENDENCIES="
    run+test:
        dev-python/packaging[>=20.0][python_abis:*(-)?]
        python_abis:3.8? (
            dev-python/tomli[>=1.0.0][python_abis:3.8]
            dev-python/typing-extensions[python_abis:3.8]
        )
        python_abis:3.9? (
            dev-python/tomli[>=1.0.0][python_abis:3.9]
            dev-python/typing-extensions[python_abis:3.9]
        )
        python_abis:3.10? (
            dev-python/tomli[>=1.0.0][python_abis:3.10]
        )
    test:
        dev-python/build[python_abis:*(-)?]
        dev-python/wheel[python_abis:*(-)?]
        dev-scm/git
        dev-scm/mercurial
    suggestion:
        dev-python/pytest[python_abis:*(-)?] [[
            description = [ required for the test suite to run ]
        ]]
        dev-python/rich[python_abis:*(-)?] [[
            description = [ Use rich as console log hander ]
        ]]
"

PYTEST_SKIP=(
    # Would need flake8
    test_dump_version_flake8
    # Wants to sign with commit and fails because there's no git repo (?)
    test_git_getdate_signed_commit
    # Test something by installing lz4 with pip, and thus wants to download
    # from the internet
    test_pip_download
    # Fails if nose is installed
    # https://github.com/pypa/setuptools_scm/issues/937
    test_pyproject_support
)

test_one_multibuild() {
    # avoid a setuptools_scm, pytest dependency loop
    if has_version dev-python/pytest[python_abis:$(python_get_abi)] ; then
        py-pep517_run_tests_pytest
    else
        ewarn "dev-python/pytest[python_abis:$(python_get_abi)] not yet installed, skipping tests."
    fi
}

