# Copyright 2021 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require pypi py-pep517 [ backend=setuptools blacklist='3.8' test=pytest ]

SUMMARY="High level compatibility layer for multiple asynchronous event loop implementations"

LICENCES="MIT"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS=""

DEPENDENCIES="
    build:
        dev-python/setuptools_scm[>=6.4][python_abis:*(-)?]
    build+run:
        dev-python/idna[>=2.8][python_abis:*(-)?]
        dev-python/sniffio[>=1.1][python_abis:*(-)?]
        python_abis:3.9? (
            dev-python/exceptiongroup[>=1.0.2][python_abis:3.9]
            dev-python/typing-extensions[>=4.1][python_abis:3.9]
        )
        python_abis:3.10? (
            dev-python/exceptiongroup[>=1.0.2][python_abis:3.10]
            dev-python/typing-extensions[>=4.1][python_abis:3.10]
        )
    test:
        dev-python/coverage[>=4.5][python_abis:*(-)?]
        dev-python/exceptiongroup[>=1.0.2][python_abis:*(-)?]
        dev-python/hypothesis[>=4.0][python_abis:*(-)?]
        dev-python/psutil[>=5.9][python_abis:*(-)?]
        dev-python/pytest-mock[>=3.6.1][python_abis:*(-)?]
        dev-python/trio[>=0.26.1][python_abis:*(-)?]
        dev-python/trustme[python_abis:*(-)?]
"

PYTEST_PARAMS=(
    # Avoid tests with e.g. DNS requests
    -m 'not network'
)
PYTEST_SKIP=(
    # Despite allowlisting they fail to connect somehow
    test_happy_eyeballs
    test_connection_refused
)

test_one_multibuild() {
    esandbox allow_net "unix:${TEMP}/pytest-of-paludisbuild/pytest-*/unix*/socket"
    esandbox allow_net "unix:${TEMP}/tmp*/socket*"
    esandbox allow_net --connect "unix:${TEMP}/tmp*/socket*"
    esandbox allow_net "unix:${TEMP}/tmp*/peer_socket"
    esandbox allow_net "unix-abstract:${TEMP}/tmp*/socket*"
    esandbox allow_net --connect "unix-abstract:${TEMP}/tmp*/socket*"
    esandbox allow_net --connect "LOCAL@5000"
    esandbox allow_net --connect "LOCAL6@5000"
    esandbox allow_net --connect "LOCAL@30000-60999"

    py-pep517_test_one_multibuild

    esandbox disallow_net --connect "LOCAL@30000-60999"
    esandbox disallow_net --connect "LOCAL6@5000"
    esandbox disallow_net --connect "LOCAL@5000"
    esandbox disallow_net --connect "unix-abstract:${TEMP}/tmp*/socket*"
    esandbox disallow_net "unix-abstract:${TEMP}/tmp*/socket*"
    esandbox disallow_net "unix:${TEMP}/tmp*/peer_socket"
    esandbox disallow_net --connect "unix:${TEMP}/tmp*/socket*"
    esandbox disallow_net "unix:${TEMP}/tmp*/socket*"
    esandbox disallow_net "unix:${TEMP}/pytest-of-paludisbuild/pytest-*/unix*/socket"
}

