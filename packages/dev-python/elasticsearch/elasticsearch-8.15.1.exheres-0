# Copyright 2020 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

MY_PN=${PN}-py

require github [ user=elastic tag=v${PV} ]
require py-pep517 [ backend=hatchling test=pytest work=${MY_PN}-${PV} ]

SUMMARY="Python client for Elasticsearch"

LICENCES="Apache-2.0"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS=""

DEPENDENCIES="
    build+run:
        dev-python/aiohttp[>=3&<4][python_abis:*(-)?]
        dev-python/elastic-transport[>=8.13&<9][python_abis:*(-)?]
        dev-python/requests[>=2.4.0&<3.0.0][python_abis:*(-)?]
    test:
        dev-python/mock[python_abis:*(-)?]
        dev-python/pytest-asyncio[python_abis:*(-)?]
        dev-python/python-dateutil[python_abis:*(-)?]
        dev-python/PyYAML[>=5.4][python_abis:*(-)?]
    suggestion:
        dev-python/orjson[python_abis:*(-)?] [[
            description = [ Optional serializer, which is faster and stricter ]
        ]]
"

PYTEST_PARAMS=(
    # Would need pyarrow, which we don't have
    --ignore=test_elasticsearch/test_serializer.py
)
PYTEST_SKIP=(
    # Would need pyarrow, which we don't have
    test_compat_serializer_used_when_given_non_compat
    test_compat_serializers_used_when_given
    test_serializer_and_serializers
)

prepare_one_multibuild() {
    py-pep517_prepare_one_multibuild

    edo sed \
        -e 's/--cov-report=term-missing --cov=elasticsearch --cov-config=.coveragerc//' \
        -i setup.cfg
}

