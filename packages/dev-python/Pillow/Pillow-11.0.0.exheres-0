# Copyright 2014 Calvin Walton <calvin.walton@kepstin.ca>
# Distributed under the terms of the GNU General Public License v2

SETUP_CFG_COMMANDS="build_ext"

MY_PN=${PN,}
MY_PNV=${MY_PN}-${PV}

require pypi
require setup-py [ import=setuptools blacklist="2 3.8" cfg_file=setup python_opts="[tk?]" work=${MY_PNV} test=pytest ]

SUMMARY="Python Imaging Library (Fork)"
DESCRIPTION="
Pillow is the \"friendly\" PIL fork by Alex Clark and Contributors. PIL
is the Python Imaging Library by Fredrik Lundh and Contributors.
"
HOMEPAGE+=" https://python-pillow.org"

LICENCES="MIT-CMU"
SLOT="0"
PLATFORMS="~amd64 ~armv7 ~armv8 ~x86"
MYOPTIONS="
    jpeg2000
    lcms
    scanner
    tiff
    webp
    X
    ( providers: ijg-jpeg jpeg-turbo ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build+run:
        media-libs/freetype:2
        sys-libs/zlib
        jpeg2000? ( media-libs/OpenJPEG:2 )
        lcms? ( media-libs/lcms2 )
        providers:ijg-jpeg? ( media-libs/jpeg:=[>=6b] )
        providers:jpeg-turbo? ( media-libs/libjpeg-turbo )
        scanner? ( dev-python/python-sane[python_abis:*(-)?] )
        tiff? ( media-libs/tiff:=[>=4] )
        webp? ( media-libs/libwebp:=[>=0.6.1] )
        X? ( x11-libs/libxcb )
    test:
        app-text/ghostscript
        dev-python/coverage[python_abis:*(-)?]
    suggestion:
        app-text/ghostscript [[ description = [ Enable EPS image plugin ] ]]
"

SETUP_CFG_build_ext_PARAMS=(
    "enable_freetype = True"
    "enable_jpeg = True"
    "enable_zlib = True"
    "disable_imagequant = True"
    "disable_raqm = True"
)

pillow_option_enable() {
    option "${1}" "enable_${2:-${1}} = True" "disable_${2:-${1}} = True"
}

setup-cfg_build_ext_append() {
    (
        pillow_option_enable jpeg2000
        pillow_option_enable lcms
        pillow_option_enable tiff
        pillow_option_enable webp
        pillow_option_enable X xcb
    ) >>setup.cfg
}

test_one_multibuild() {
    local pytest_params=(
        # Avoid pulling in PyQt/PySide for tests, needs X
        --ignore Tests/test_qt_image_qapplication.py
        --ignore Tests/test_qt_image_toqimage.py
        # Needs X
        --ignore Tests/test_imagemorph.py
        # Tests below are skipped on a "normal" system, but not on CI,
        # and they fail there.
        --ignore Tests/test_image.py
        --ignore Tests/test_imageshow.py
        --ignore Tests/test_tiff_crashes.py
    )

    if option jpeg2000 ; then
        :
    else
        # Both tests fail when the jpeg2000 option is disabled, complaining
        # about lacking support for that.
        pytest_params+=(
            -k 'not test_icns_decompression_bomb and not test_pathlib'
        )
    fi

    # ImportError: cannot import name '_imagingcms' from 'PIL'
    option lcms || pytest_params+=( --ignore Tests/test_image_convert.py )

    option tiff || pytest_params+=( --ignore Tests/test_file_tiff.py --ignore Tests/test_tiff_ifdrational.py )

    # https://github.com/python-pillow/Pillow/issues/6710
    option X || pytest_params+=( --ignore Tests/test_imagegrab.py )

    PYTHONPATH=$(echo "${PWD}"/build/lib*) edo ${PYTHON} -B selftest.py --installed
    PYTHONPATH="$(ls -d ${PWD}/build/lib*)" edo py.test-$(python_get_abi) \
        "${pytest_params[@]}"
}

